## Question and Answers
Denken Sie, dass sie fit für den Support sind? Testen Sie ihr wissen in Deutsch und Englisch.


| Fragen                                 | Antwort                                                 |
|-----------------------------------------------|----------------------------------------------------------|
| Sie erhalten einen Anruf, der Kunde schreit Sie wegen der langen Wartezeit an. Sie wollen sich verteidigen und versuchen, die Situation zu erklären. Haben Sie entsprechend gehandelt? | Nein, das Beste, was man tun kann, ist, ruhig zu bleiben. Sagen Sie, dass es Ihnen leid tut, fühlen Sie sich in den Anrufer mit und hören Sie zu. Fals nötig endschuldigen sie sich für alle unanelmichkeiten die Endstanden sind.|
| Wie können Sie dem Anrufer mitteilen, dass Sie noch zuhören? | Machen Sie kleine Geräusche wie "mhm", "Ja" oder ähnliches. | 
| Nennen Sie eine Taktik, um sicherzustellen, dass der Anrufer die Lösung versteht | Bleiben Sie bei kleinen Aufgaben im Anruf, verwenden Sie eine "einfache" Sprache um die Lösung zu erklären, achten sie aber darauf das sie nicht zu einfach sind wenn der Anrufer erfahren ist.|
| Ein Anrufer kann die Anweisung, die Sie ihm geben, nicht befolgen. Was ist das Wichtigste, woran er sich erinnern sollte? | Schieben Sie die Schuld nicht auf den Anrufer. Erlären sie die Anweißung noch mal leichter als zu vor. |
| Die Zufridenheit der Anrufer sind inkonsistenz. Was ist das Beste, was man tun kann? | Das Team sollte eine neue Leitlinie entwickeln. Wenn eine bereits implantiert ist, sollten sie strenger durchgesetzt werden. |
| Bringen Sie sie in die richtige Reihenfolge: Verabschiedung und Nachbereitung, Umgang mit Einwänden, Bedarfsanalyse, Vorbereitung, Abschluss und Handlungsaufforderung, Lösungspräsentation, Begrüßung und Beziehungsaufbau,| Vorbereitung, Begrüßung und Beziehungsaufbau, Bedarfsanalyse, Lösungspräsentation, Umgang mit Einwänden, Abschluss und Call-to-Action, Verabschiedung und Nachbereitung. |
| Was passiert in der Bedarfsanalyse? | Stellen Sie gezielte Fragen, um die Bedürfnisse des Anrufers zu verstehen: Hören Sie aktiv zu, finden Sie den Grund für das Problem. |
| Was sollte im letzten Schritt eines jeden Anrufs passieren? | Nachdem der Anrufer aufgelegt hat, denken Sie darüber nach. Was war gut? Was ist schief gelaufen? Wie kann man es besser machen? |
| Sollten Sie jeden Anrufer gleich behandeln? | Nein passen Sie sich  an das vorhandene Wissen jedes Anrufers individuell an. Vergessen Sie "einfache" Lösungen für erfahrene Anrufer und geben Sie mehr Wissen an diejenigen, die nicht so Wissend sind. |








| Question                                      | Answers                                                  |
|-----------------------------------------------|----------------------------------------------------------|
| You receive a call the costumer starts yelling at you because of the long wait. You want to defend yourself and try to explain the situation. Did you act accordingly? | No the best thing to do is to stay calm. Say that you are sorry empathize with the caller and listen. Apoligize if nesesary. |
| How can you inform the caller that you are still listening? | Make little noises as "mhm" "Yes" or similar. | 
| Name one tactic to  insure the caller understudied the solution? | Stay on the call for small tasks, use "simple" language, but keep in mindt that how Knowlagebel the caller is and adabt your responce with that. |
| A caller dose can´t follow the instruction you give him what is the most important thing to remember? | Don`t imply the fault at the caller, explain it agin but make it simpler. |
| The Feedback of the callers discripe a Qualaty difrence between calls. What’s the best thing to do? | The Team should develop a new caller guideline. If one is already implanted they should be stricter enforced.|
| Bring them in the correct order: Farewell and follow-up, Handling objections, Needs analysis, Preparation, Closing and call to action, Solution presentation, Greeting and relationship building.| Preparation, Greeting and relationship building, Needs analysis, Solution presentation, Handling objections, Closing and call to action, Farewell and follow-up. |
| What happens in in the Needs analysis?| Ask targeted Questions to understand the needs of the Caller listen actively find the reason for the problem. |  
| What should happen in the last step of every call? | After the Caller hung up make a reflection about it. What was good? What went wrong? How can you do it better? |
| Should you treat every Caller the same? | No adapt to the existing knowledge of every caller. Forget "Simple" Solutions for experience ones and give more knowledge to the ones how are not as knowledge. |
