# Deeskalierende Strategien

- Ruhe bewahren: Bleibe gelassen, um die Situation zu entspannen.
- Aktives Zuhören: Zeige Verständnis, wiederhole die Aussagen der Person.
- Empathie zeigen: Bestätige Emotionen, um Verständnis zu signalisieren.
- Nicht defensiv reagieren: Betrachte die Situation aus der Perspektive der anderen Person.
-  Verwendung beruhigender Sprache: Vermeide aggressive Worte.
- Klärung und Zusammenfassung: Stelle sicher, dass du die Person richtig verstanden hast.
- Lösungsorientierung: Fokussiere auf Lösungen und Kompromisse.
-  Zeit und Raum geben: Gewähre Zeit zur Beruhigung, wenn nötig.
- Alternative Perspektiven: Bring andere Sichtweisen ein.
- Professionalität wahren: Bleibe professionell trotz emotionaler Intensität.