# Aktiv zu hören und professioneller Umgang

## Aktives Zuhören

- Konzentration ohne Ablenkung:
  Schaffe eine ruhige Umgebung ohne Störungen, um dich vollständig auf den Kunden und sein Anliegen zu konzentrieren.
  
- Wiederholung und Zusammenfassung:
  Wiederhole oder fasse in eigenen Worten zusammen, was der Kunde gesagt hat, um sicherzustellen, dass du seine 
  Aussagen richtig verstanden hast.

- Nachfragen stellen:
  Stelle gezielte Fragen, um mehr Details oder Klarstellungen zu den Aussagen des Kunden zu erhalten, ohne das Gespräch zu unterbrechen.

## Höfliche und respektvolle Kommunikation

- Verwende eine höfliche und respektvolle Sprache, um eine professionelle Atmosphäre zu schaffen und dem Kunden das 
  Gefühl zu geben, wertgeschätzt zu werden.

- Einfühlungsvermögen zeigen:
  Zeige Verständnis für die Bedürfnisse und Emotionen des Kunden, indem du empathisch auf seine Situation eingehst.

- Lösungsorientierung statt Schuldzuweisungen:
  Fokussiere dich auf Lösungen und Möglichkeiten, anstatt Schuld zuzuweisen oder dich defensiv zu verhalten, falls Probleme auftreten.

- Geduld und Ruhe bewahren:
  Bleibe ruhig und geduldig, auch wenn der Kunde frustriert oder unzufrieden ist. Das zeigt deine Professionalität und deine Fähigkeit, in schwierigen Situationen zu agieren.

- Klare Kommunikation von Erwartungen:
  Kläre klar die Erwartungen des Kunden und gib realistische Zeitrahmen oder Lösungen an.
  
- Dankbarkeit und Abschied:
  Bedanke dich beim Kunden für sein Vertrauen und verabschiede dich höflich, um einen positiven Eindruck zu - hinterlassen.

|Phase    |   	|   	|   	|   	|
|---	    |---	|---	|---	|---	|
|     	  |   	|   	|   	|   	|
|   	    |   	|   	|   	|   	|
|   	    |   	|   	|   	|   	|