## Allgemein

- Hauptsache geschlossenen Fragen oder kurze Antwort Fragen -> schnellere Abarbeitung
- Support in Deutsch und Englisch anbieten
- Fragen gelten nur als Leitfaden und zur Orientierung

## Regelmäßige Reflexion: 
- monatliche Meetings
- Einholen von Feedback vom Kunden, um aus der Erfahrung zu lernen und zukünftige Interaktionen zu verbessern.
- Angebot von Formularen zur Feedbackerfassung und Möglichkeit, Kundengespräche aufzuzeichnen.
- einzelne Gespräche aufnehmen annonym aufnehmen um zu gucken wie es ist
- Selbstreflexion







